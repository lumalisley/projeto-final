Given ('acesso ao site e que exista alguma tarefa a ser executada na lista para ser excluida', () => {
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
    
    
})
When ('excluir a {string} a ser executada', (Tarefa) => {
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/input').type (Tarefa) // informar o nome a ser adicionado
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/div').click() // clicar para adicionar
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/input').trigger('mouseover') // passar mouse em cima de escrever
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li/div[1]').click() // clicar para escrever completo
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li/div[2]/button').click() // clicar em excluir
})
Then('aparecer na tela o Item deletado das tarefas excluidas', () => { 
    cy.xpath('//*[@id="root"]/div[3]/div/div/div/div/div') // verificar se aparece o pop-up Item deletado
    .should('be.visible')   // verificado item na página
}) 
