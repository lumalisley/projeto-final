
Given ('acesso a página das minhas tarefas e que já exista um item adicionado', () => {
    cy.visit('/')
})
When ('tento marcar um {string} para o dia todo', (item) => {
    cy.contains('Add Task').click() //clicar em add tarefa
    cy.get('div[class="Avatar_badge__unosJ"]').click() // icone
    cy.xpath('/html/body/div[2]/div/div/div/div[2]/ul/li[3]/a').click()
    cy.get('div[class="TodoForm_category__30vm6"]').click()
    cy.get('a[class="bp3-menu-item bp3-popover-dismiss CategoryMenu_category__2CQAk CategoryMenu_2__2aC0h"]').click()
    cy.get('input[name="description"]').click().type(item)
    cy.get('input[name="location"]').click().type('No parque')
    cy.get('input[type="text"]').click()
    cy.xpath('/html/body/div[4]/div/div/div/div[2]/div/div/div[1]/div/div[2]/div/div[3]/div[5]/div[1]').click() // data
    cy.get('span[class="bp3-control-indicator"]').click() // opção dia todo
    cy.get('button[type="submit"]').click()
    cy.xpath('//*[@id="root"]/div/div/div[3]/div/div[1]/button/span').click() // tela inicial
    cy.contains('Today').click()
    cy.contains('Nov 29th').click()

})
Then('é exibido o item marcado para o dia todo', () => { //verificar esse then para visualizar All Today
    cy.get('div[class="TodoListItem_subTitle__fB1aK"]')
    .should('be.visible')
    //cy.screenshot()
})  