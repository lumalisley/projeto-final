#language:pt
Funcionalidade: adicionar e organizar itens no ToDo APP
Para marcar os itens a serem executados como concluídas 
Como organizar a lista TO DO
Eu quero ter controle sobre as tarefas que necessito executar

#Regra de Negócio 01: É possível adicionar itens na lista TODO
Esquema do Cenário: item adicionado na lista TO DO 
    Dado acesso a página das minhas tarefas
    Quando tento inserir um "<item>" a ser executado e "<onde>"
    Então é exibido a tela inicial 
Exemplos:
| item                  | onde            | 
| Fazer Projeto Solutis | casa            | 
| Comer                 | na rua          |

#Regra de Negócio 02: É possível escolher uma data para visualizar as tarefas
Esquema do Cenário: data escolhida para visualizar tarefas
    Dado acesso a página das minhas tarefas e que já exista um "<item>" adicionado 
    Quando tento visualizar um "<item>" e "<onde>"
    Então é exibido todos os itens feitos
Exemplos:
| item                  | onde            | 
| Estudar               | em casa         | 
| Tomar banho           | em casa         |

#Regra de Negócio 03: É possível adicionar itens com a mesma descrição
Esquema do Cenário: item com a mesma descrição 
    Dado acesso a página das minhas tarefas e que já exista um "<item>" adicionado
    Quando tento inserir um "<item>" e "<onde>" com a mesma descrição 
    Então é exibido os itens com a mesma descrição
    E é exibido a quantidade de tarefas
Exemplos:
| item                  | onde         | 
| Fazer Projeto Solutis | Biblioteca   | 

#Regra de Negócio 04: É possível marcar todos os itens como feitos da lista TODO  
Esquema do Cenário: itens marcados como feitos 
    Dado acesso a página das minhas tarefas e que já exista um "<item>" adicionado
    Quando tento marcar os "<itens>" como feitos
    Então é exibido os itens marcados como feitos
    E é exibido uma tarefa a menos na quantidade de tarefas
Exemplos:
| itens                  | onde            | 
| Estudar                | Biblioteca      |

#Regra de Negócio 05: É possível excluir um item da lista 
Esquema do Cenário: item existente é excluído
    Dado acesso a página das minhas tarefas e que já exista um "<item>" adicionado 
    Quando tento excluir um "<item>" 
    Então o "<item>" é excluído 
Exemplos:
| item                  | onde            | 
| Estudar               | Biblioteca      |

#Regra de Negócio 06: É possível desfazer item selecionado como feito 
Esquema do Cenário: itens marcados como completos são desfeitos
    Dado acesso a página das minhas tarefas e que já exista um "<item>" adicionado 
    Quando tento desmarcar um "<item>" já completo 
    Então o "<item>" é desmarcado
Exemplos:
| item                  | 
| Estudar               | 

#Regra de Negócio 07: É possível excluir todos os itens de uma só vez
Esquema do Cenário: itens existentes são excluídos 
    Dado acesso a página das minhas tarefas e que já exista um item adicionado 
    Quando tento excluir os "<itens>" 
    Então os "<itens>" são excluídos
Exemplos:
| itens                 | onde            | 
| Estudar               | Biblioteca      |

#Regra de Negócio 08: É possível visualizar itens ainda ativos
Esquema do Cenário: itens ainda ativos 
    Dado acesso a página das minhas tarefas e que já exista um item adicionado 
    Quando tento visualizar os "<itens>" ativos 
    Então os "<itens>" ativos são exibidos
Exemplos:
| itens                 |
| Estudar               | 

#Regra de Negócio 09: É possível visualizar todos os itens já feitos
Esquema do Cenário: itens já feitos 
    Dado acesso a página das minhas tarefas e que já exista um item adicionado 
    Quando tento visualizar os "<itens>" já feitos 
    Então os "<itens>" já feitos são exibidos
Exemplos:
| itens                 |
| Comer                 | 

#Regra de Negócio 10: É possível escolher horário do item
Esquema do Cenário: item com hora definida
    Dado acesso a página das minhas tarefas e que já exista um item adicionado
    Quando tento escolher a hora de um "<item>" 
    Então é exibido o item com hora definida
Exemplos:
| item                  | 
| Atividade Física      |

#Regra de Negócio 11: É possível adicionar item para o dia inteiro
Esquema do Cenário: item marcado para o dia todo
    Dado acesso a página das minhas tarefas e que já exista um item adicionado
    Quando tento marcar um "<item>" para o dia todo 
    Então é exibido o item marcado para o dia todo
Exemplos:
| item                  | 
| Atividade Física      | 

#Regra de Negócio 12: É possível marcar um item da lista como feito
Esquema do Cenário: item marcado como feito 
    Dado acesso a página das minhas tarefas e que já exista um "<item>" adicionado
    Quando tento marcar o "<item>" como feito
    Então é exibido o item marcado como feito
    E é exibido uma tarefa a menos na quantidade de tarefas
Exemplos:
| item                   | onde            | 
| Estudar                | Biblioteca      |
