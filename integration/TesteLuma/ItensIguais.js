Given ('que exista dois itens iguais na lista TO DO', () => {
    cy.visit('https://just-another-todo-app.now.sh/')
})
When ('tento adicionar dois itens {string}', (item) => {
    cy.get('[id="new-todo-item"]').type (item) // informar o nome a ser adicionado
    cy.get('[data-testid="form-submit"]').click() // clicar para adicionar
    cy.get('[id="new-todo-item"]').type (item) // informar o nome a ser adicionado
    cy.get('[data-testid="form-submit"]').click() // clicar para adicionar
})
Then('os dois itens iguais', () => { 
    cy.get('[data-testid="form-item-item-completion"]') // verificar se o nome está na página
    .should('be.visible') // Verifica se o elemento está visível na página
}) 