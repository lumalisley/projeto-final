
Given ('acesso a página das minhas tarefas', () => {
    cy.visit('/')
})
When ('tento inserir um {string} a ser executado e {string}', (item, onde) => {
    cy.contains('Add Task').click() //clicar em add tarefa
    cy.get('div[class="Avatar_badge__unosJ"]').click() // icone
    cy.xpath('/html/body/div[2]/div/div/div/div[2]/ul/li[3]/a').click()
    cy.get('div[class="TodoForm_category__30vm6"]').click()
    cy.get('a[class="bp3-menu-item bp3-popover-dismiss CategoryMenu_category__2CQAk CategoryMenu_2__2aC0h"]').click()
    cy.get('input[name="description"]').click().type(item)
    cy.get('input[name="location"]').click().type(onde)
    cy.get('input[type="text"]').click()
    cy.xpath('/html/body/div[4]/div/div/div/div[2]/div/div/div[1]/div/div[2]/div/div[3]/div[5]/div[1]').click()
    cy.get('button[type="submit"]').click()
    cy.xpath('//*[@id="root"]/div/div/div[3]/div/div[1]/button/span').click()
})
Then('é exibido a tela inicial', () => { 
cy.get('[id="root"]')
.should('be.visible')
//cy.screenshot()
})  