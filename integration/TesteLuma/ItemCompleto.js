Given ('existem itens na lista TO DO', () => {
    cy.visit('https://just-another-todo-app.now.sh/')
})
When ('tento marcar os itens {string}', (item) => {
    cy.get('[id="new-todo-item"]').type (item) // informar o nome a ser adicionado
    cy.get('[data-testid="form-submit"]').click() // clicar para adicionar
    cy.get('[data-testid="form-item-listitem"]').click() // clicar no item de escrever
    // cy.get('class="svg-inline--fa fa-square fa-w-14 fa-2x "')

})
Then('itens marcados {string}', (resultado) => { 
    cy.get('[data-testid="form-item-item-completion"]') // verificar se o nome está na página
    .should('be.visible')   // verificado item na página
})  
