// #adicionar sem as perferências ativas
Given ('acesso ao site e que não exista nenhuma tarefa a ser executada', () => {
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
})
When ('inserir uma {string} a ser executada', (tarefa) => {
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/input').type (tarefa) // informar o nome a ser adicionado
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/div').click() // clicar para adicionar
    }
)
Then('o sistema deve adicionar o {string} a ser executada', (resultado) => { 
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li/div[1]/div[2]') // verificar se o nome está na página
        .should('be.visible') // Verifica se o elemento está visível na página
    }
)  