Given ('acesso ao site e que exista alguma tarefa a ser executada na lista', () => {
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
})
When ('editar a {string} a ser executada para o {string}', (Tarefa, Resultado) => {
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/input').type (Tarefa) // informar o nome a ser adicionado
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/div').click() // clicar para adicionar
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/input').trigger('mouseover') // passar mouse em cima de escrever
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li/div[1]').click() // clicar para escrever completo
    cy.get('input[class="MuiInputBase-input MuiInput-input"]').clear({force: true}).type(Resultado);
    cy.xpath('//*[@id="root"]/div[1]/header/div').click()
})
Then('aparecer na tela o {string} das tarefas editadas', (Resultado) => { 
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li/div[1]/div[2]') // verificar se o nome está na página
    .should('be.visible') // Verifica se o elemento está visível na página
})  