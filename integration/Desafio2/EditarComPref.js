Given ('acesso ao site e que exista alguma tarefa a ser executada na lista', () => {
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
})
When ('editar a {string} a ser executada para o {string} 2e', (Tarefa, Resultado) => {
        cy.xpath('//*[@id="root"]/div[1]/header/div/button').trigger('mouseover').click({force: true})
        cy.xpath('/html/body/div[2]/div[3]/ul[1]/div[1]/div[1]/span/span[1]/span[1]').click() // clicar nas preferencias
        cy.xpath('/html/body/div[2]/div[3]/ul[1]/div[2]/div[1]/span/span[1]/span[1]').click()
        cy.xpath('/html/body/div[2]/div[3]/ul[1]/div[3]/div[1]/span/span[1]/span[1]').click()
        cy.get('div[class="MuiBackdrop-root"]').click()
        cy.xpath('//*[@id="root"]/div[1]/div[3]').click()
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/input').type (Tarefa) // informar o nome a ser adicionado
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/div').click() // clicar para adicionar
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/input').trigger('mouseover') // passar mouse em cima de escrever
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li/div[1]').click() // clicar para escrever completo
        cy.get('input[class="MuiInputBase-input MuiInput-input"]').clear({force: true}).type(Resultado);
    }
)
Then('aparecer na tela o {string} das tarefas editadas 3e', (Resultado) => { 
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li') // verificar se o nome está na página
        .should('be.visible') // Verifica se o elemento está visível na página
    }
) 