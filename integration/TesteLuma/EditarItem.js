// #Regra: É possível editar um item da lista

Given ('que exista um item na lista TO DO', () => {
    cy.visit('https://just-another-todo-app.now.sh/')
    
    
})
When ('tento editar o item {string} com o novo item {string}', (item, novoItem) => {
    cy.get('[id="new-todo-item"]').type (item) // informar o nome a ser adicionado
    cy.get('[data-testid="form-submit"]').click() // clicar para adicionar
    cy.get('[data-testid="form-item-listitem"]').trigger('mouseover')  // clicar no item de escrever
    cy.get('[data-testid="property-bar-edit-button"]').click()  // editar
    cy.get('[id="input-edit-todo-item"]').clear({force: true}).type (novoItem) // colocar mouse em cima do item
    cy.get('[data-testid="form-edit-submit-button"]').click() // clicar para salvar
    
})

Then('o item {string}', (resultado) => { 
    cy.get('[data-testid="form-item-item-completion"]') // verificar se o nome está na página
    .should('be.visible')   // verificado item na página
}) 