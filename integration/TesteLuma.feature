#language: pt
Funcionalidade: Organizar itens na lista TO DO
    COMO organizador de lista TO DO
    DESEJO organizar os itens na minha lista
    PARA que eu tenha controle sobre o que preciso fazer

    #Regra: Todo item tem uma descrição
    Esquema do Cenário: Novo item <resultado> à lista TO DO
        Dado que não exista nenhum item na lista TO DO
        Quando tento adicionar o item "<novo item>"
        Então o item "<resultado>"      
        Exemplos:
            | novo item           | resultado        | 
            |                     | não é adicionado | 
            | Fazer projeto final |  é adicionado    |
            | Comer               |  é adicionado    |


    #Regra: É possível editar um item da lista
    Esquema do Cenário: Editar Novo item <resultado> à lista TO DO 
        Dado que exista um item na lista TO DO
        Quando tento editar o item "<item>" com o novo item "<novo item>"
        Então o item "<resultado>"      
        Exemplos:
            |  item               | novo item        | resultado        |
            | Fazer projeto final |  Projeto final Ok| Projeto final Ok |
            | Testar Projeto      | Tudo Ok          | Tudo Ok          |


    #Regra: É possível excluir itens da lista TO DO
    Esquema do Cenário: Excluir item <resultado> da lista TO DO 
        Dado que exista um item na lista TO DO
        Quando tento excluir o item "<item>" 
        Então o item Exluido "<resultado>"      
        Exemplos:
            | item                | resultado        |
            | Fazer projeto final | Item Excluido    |
            | Testar Projeto      | Item Excluido    |


    #Regra: É possível ter itens com a mesma descrição
    Esquema do Cenário: Dois itens iguais <resultado> à lista TO DO
        Dado que exista dois itens iguais na lista TO DO
        Quando tento adicionar dois itens "<novo item>"
        Então os dois itens iguais       
        Exemplos:
            | novo item           |  
            | Comer               |  
            | Trabalhar           |  

    #Regra: É possível marcar como completo um item da lista
    Esquema do Cenário: Marcar item como completo <resultado> à lista TO DO
        Dado existem itens na lista TO DO
        Quando tento marcar os itens "<novo item>"
        Então itens marcados "<resultado>"      
        Exemplos:
            | novo item           | resultado        | 
            | Comer               |  é adicionado    |
            | Trabalhar           |  é adicionado    |