Given ('acesso a página das minhas tarefas e que já exista um {string} adicionado', () => {
    cy.visit('/')
})
When ('tento marcar o {string} como feito', (item, onde) => {
    cy.contains('Add Task').click() //clicar em add tarefa
    cy.get('div[class="Avatar_badge__unosJ"]').click() // icone
    cy.xpath('/html/body/div[2]/div/div/div/div[2]/ul/li[3]/a').click()
    cy.get('div[class="TodoForm_category__30vm6"]').click()
    cy.get('a[class="bp3-menu-item bp3-popover-dismiss CategoryMenu_category__2CQAk CategoryMenu_2__2aC0h"]').click()
    cy.get('input[name="description"]').click().type(item)
    cy.get('input[name="location"]').click().type('Biblioteca')
    cy.get('input[type="text"]').click()
    cy.xpath('/html/body/div[4]/div/div/div/div[2]/div/div/div[1]/div/div[2]/div/div[3]/div[5]/div[1]').click()
    cy.get('button[type="submit"]').click()
    cy.xpath('//*[@id="root"]/div/div/div[3]/div/div[1]/button/span').click() // tela inicial
    cy.contains('Today').click()
    cy.contains('Nov 29th').click()
    //cy.xpath('//*[@id="root"]/div/div/div[2]/div/div[3]/div[2]/div/span[1]/span/button').click()
    cy.xpath('//*[@id="root"]/div/div/div[2]/div/div[2]/div/div/div[3]/div/div/button[1]').click({force: true}) //marcar item
    cy.wait(1000)
})
Then('é exibido o item marcado como feito', () => { 
cy.get('div[class="Avatar_root__240je  "]')
.should('be.visible')
})  
And('é exibido uma tarefa a menos na quantidade de tarefas', () => { 
    cy.get('div[class="MyDay_totals__1_XO4"]')
    .should('be.visible')
    //cy.screenshot()
}) 