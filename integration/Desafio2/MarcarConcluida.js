Given ('acesso ao site e que exista alguma tarefa a ser executada', () => {
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
})
When ('marcar a {string} como concluída', (tarefa) => {
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/input').type (tarefa) // informar o nome a ser adicionado
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/div').click() // clicar para adicionar
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li/div[1]/div[1]/span/span[1]/input').click() // clicar item completo
})
Then('aparecer na tela as tarefas adicionadas', () => { 
    cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li/div[1]/div[2]') // verificar se o nome está na página
    .should('be.visible') // Verifica se o elemento está visível na página
})  
