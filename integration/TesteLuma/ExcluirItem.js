// #Regra: É possível excluir itens da lista TO DO

Given ('que exista um item na lista TO DO', () => {
    cy.visit('https://just-another-todo-app.now.sh/')
    
    
})
When ('tento excluir o item {string}', (item) => {
    
    cy.get('[id="new-todo-item"]').type (item) // informar o nome a ser adicionado
    cy.get('[data-testid="form-submit"]').click() // clicar para adicionar
    cy.get('[data-testid="form-item-listitem"]') // colocar mouse em cima do item
    cy.get('[data-testid="form-item-item-completion"]').trigger('mouseover')  // clicar no item de escrever
    cy.get('[data-testid="property-bar-delete-button"]').click()  // clicar em excluir
    
})

Then('o item Exluido {string}', (resultado) => { 
    cy.get('[data-testid="form-item-listidem"]') // verificar se o nome está na página
    .should('not.be.visible')   // verificado item na página
}) 

Then('o item Exluido {string}', (resultado) => { 
    if(resultado === "Item excluido"){
        cy.get('[data-testid="form-item-listidem"]') // verificar se o nome está na página
        .should('not.be.visible') // verificado que o item não esta na página
    }else{
        cy.get('[data-testid="form-item-item-completion"]') // verificar se o nome está na página
        .should('be.visible') // Verifica se o elemento está visível na página
    }
})