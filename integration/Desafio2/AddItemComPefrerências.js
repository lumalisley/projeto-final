Given ('acesso ao site e que não exista nenhuma tarefa a ser executada', () => {
    cy.visit('https://mastro-elfo.github.io/todo-react/#/dashboard')
})
When ('inserir uma {string} a ser executada 1a', (tarefa) => {
        cy.xpath('//*[@id="root"]/div[1]/header/div/button').trigger('mouseover').click({force: true})
        //cy.get('input[class="MuiIconButton-label"]').click() // clicar para add preferencias
        cy.xpath('/html/body/div[2]/div[3]/ul[1]/div[1]/div[1]/span/span[1]/span[1]').click() // clicar nas preferencias
        cy.xpath('/html/body/div[2]/div[3]/ul[1]/div[2]/div[1]/span/span[1]/span[1]').click()
        cy.xpath('/html/body/div[2]/div[3]/ul[1]/div[3]/div[1]/span/span[1]/span[1]').click()
        cy.get('div[class="MuiBackdrop-root"]').click() // clicar fora 
        cy.xpath('//*[@id="root"]/div[1]/div[3]').click()
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/input').type (tarefa) // informar o nome a ser adicionado
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[1]/li/div/div/div').click() // clicar para adicionar
    }
)
Then('aparecer na tela as tarefas adicionadas 2a', () => { 
        cy.xpath('//*[@id="root"]/div[1]/div[3]/div/ul[2]/li') // verificar se o nome está na página
        .should('be.visible') // Verifica se o elemento está visível na página
    }
)  