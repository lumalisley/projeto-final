// #Regra: Todo item tem uma descrição

Given ('que não exista nenhum item na lista TO DO', () => {
    cy.visit('https://just-another-todo-app.now.sh/')
    cy.get('[id="new-todo-item"]').click()
})
When ('tento adicionar o item {string}', (item) => {
    if(item === ""){
        cy.get('[data-testid="form-submit"]').click({force: true})
    }else{
        cy.get('[id="new-todo-item"]').type (item) // informar o nome a ser adicionado
        cy.get('[data-testid="form-submit"]').click() // clicar para adicionar
    }
})
Then('o item {string}', (resultado) => { 
    if(resultado === "não é adicionado"){
        cy.get('[data-testid="form-item-item-completion"]') 
        .should('not.be.visible')
    }else{
        cy.get('[data-testid="form-item-item-completion"]') // verificar se o nome está na página
        .should('be.visible') // Verifica se o elemento está visível na página
    }
})  
