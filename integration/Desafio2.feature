#language:pt
Funcionalidade: adicionar itens a serem executados para marcar com concluidas ou excluir
Para marcar os itens a serem executados como concluídas
Como organizar a lista TO DO
Eu quero ter controle sobre as tarefas que necessito executar

#RN: adicionar o item na lista TO DO
Esquema do Cenário: adicionar tarefas a serem executadas  
    Dado acesso ao site e que não exista nenhuma tarefa a ser executada
    Quando inserir uma "<tarefa>" a ser executada
    Então o sistema deve adicionar o "<resultado>" a ser executada
    
Exemplos:
| tarefa                | resultado       | 
| Fazer Projeto Solutis | Item adicionado | 
| Comer                 | Item adicionado | 

#RN: marcar como concluído o item na lista TO DO
Esquema do Cenário: marcar tarefas a serem executadas 
    Dado acesso ao site e que exista alguma tarefa a ser executada
    Quando marcar a "<Tarefa>" como concluída
    Então aparecer na tela as tarefas adicionadas
Exemplos:
| Tarefa                | 
| Fazer Projeto Solutis | 
| Comer                 |

#RN: editar o item na lista TO DO
Esquema do Cenário: editar tarefas a serem executadas 
    Dado acesso ao site e que exista alguma tarefa a ser executada na lista
    Quando editar a "<Tarefa>" a ser executada para o "<Resultado>"
    Então aparecer na tela o "<Resultado>" das tarefas editadas

Exemplos:
| Tarefa                | Resultado                 | 
| Fazer Projeto Solutis | Não quero fazer nada hoje | 
| Comer                 | Comer fitness             | 

#RN: excluir o item na lista TO DO
Esquema do Cenário: excluir tarefas a serem executadas 
    Dado acesso ao site e que exista alguma tarefa a ser executada na lista para ser excluida
    Quando excluir a "<Tarefa>" a ser executada 
    Então aparecer na tela o Item deletado das tarefas excluidas

Exemplos:
| Tarefa                | 
| Fazer Projeto Solutis | 
| Comer                 | 

#RN: adicionar o item com as preferências ativas -filtro, progresso, show excluído-
Esquema do Cenário: adicionar tarefas a serem executadas com as preferências ativas
    Dado acesso ao site e que não exista nenhuma tarefa a ser executada
    Quando inserir uma "<tarefa>" a ser executada 1a
    Então aparecer na tela as tarefas adicionadas 2a
    
Exemplos:
| tarefa                | Resultado       | 
| Fazer Projeto Solutis | Item adicionado | 
| Comer                 | Item adicionado | 

#RN: marcar como concluído o item com as perferências ativas
Esquema do Cenário: marcar tarefas a serem executadas com as preferências ativas
    Dado acesso ao site e que exista alguma tarefa a ser executada
    Quando marcar a "<Tarefa>" como concluída 2m
    Então aparecer na tela as tarefas adicionadas e a barra colorida preenchida 3m
Exemplos:
| Tarefa                | 
| Fazer Projeto Solutis | 
| Comer                 |

#RN: editar o item com as perferências ativas
Esquema do Cenário: editar tarefas a serem executadas com as preferências ativas
    Dado acesso ao site e que exista alguma tarefa a ser executada na lista
    Quando editar a "<Tarefa>" a ser executada para o "<Resultado>" 2e
    Então aparecer na tela o "<Resultado>" das tarefas editadas 3e

Exemplos:
| Tarefa          | Resultado          | 
| Fazer almoço    | Vou comprar pronto | 
| Lavar roupa     | Deixar suja        | 

#RN: excluir o item com as perferências ativas
Esquema do Cenário: excluir tarefas a serem executadas com as preferências ativas
    Dado acesso ao site e que exista alguma tarefa a ser executada na lista para ser excluida 1x
    Quando excluir a "<Tarefa>" a ser executada 2x
    Então aparecer na tela o pop-up

Exemplos:
| Tarefa                | Resultado       | 
| Fazer Projeto Solutis | Item Excluido   | 
| Comer                 | Item Excluido   | 


#RN: excluir o item com as perferências ativas e desfazer a exclusão
Esquema do Cenário: excluir tarefas a serem executadas com as preferências ativas e executar botão de desfazer
    Dado acesso ao site e que exista alguma tarefa a ser executada na lista para ser excluida 1x
    Quando excluir a "<Tarefa>" a ser executada 2x
    Então aparecer na tela o pop-up item deletado
    E clicar na opção desfazer
    Então aparecer a "<Tarefa>" novamente

Exemplos:
| Tarefa                | 
| Fazer Projeto Solutis | 
| Comer                 | 

#RN: clicar em ajuda
Cenário: clicar em gitHub para ajuda
    Dado acesso ao site e que exista não exista alguma tarefa a ser executada na lista 
    Quando clicar noa opção do GitHub tutorial
    Então aparece uma nova página
# esse cenario não entrou na verificação pq?

#RN: clicar no botao sobre 
Cenário: clicar em sobre para ver configurações
    Dado acesso ao site e que exista não exista alguma tarefa a ser executada na lista 
    Quando clicar na opção das configurações
    Então aparece a página das configurações 
#esse cenario não entrou na verificação pq?